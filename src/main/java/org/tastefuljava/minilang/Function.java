package org.tastefuljava.minilang;

public class Function {
    public final int paramCount;
    public final Expression body;

    public Function(int paramCount, Expression body) {
        this.paramCount = paramCount;
        this.body = body;
    }
}
