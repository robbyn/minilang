package org.tastefuljava.minilang;

import java.util.Map;

public interface Expression {
    public int evaluate(Map<String,Function> table, int... parms);

    public static Expression number(int x) {
        return (table, parms) -> x;
    }

    public static Expression add(Expression a, Expression b) {
        return (table, parms) ->
                a.evaluate(table, parms) + b.evaluate(table, parms);
    }

    public static Expression mul(Expression a, Expression b) {
        return (table, parms) ->
                a.evaluate(table, parms) * b.evaluate(table, parms);
    }

    public static Expression call(String name, Expression... parms) {
        return (table, oldParms) -> {
            Function f = table.get(name);
            if (f == null) {
                System.out.println("Unknown function " + name);
                return 0;
            } else if (f.paramCount != parms.length) {
                System.out.println("Wrong number of parameters for function "
                    + name + ": expected " + f.paramCount
                    + ", actual " + parms.length);
                return 0;
            }
            int[] newParms = new int[parms.length];
            for (int i = 0; i < parms.length; ++i) {
                newParms[i] = parms[i].evaluate(table, oldParms);
            }
            return f.body.evaluate(table, newParms);
        };
    }

    public static Expression param(int index) {
        return (table, parms) -> parms[index];
    }
}
